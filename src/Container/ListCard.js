import React, { Component, Fragment } from 'react';
import axios from 'axios';
import Card from '../Component/Card';


class ListCard extends Component{
	state = {
		card: [],
  }

	componentDidMount(){
	 axios.get('http://192.168.52.85:5000/api/TodoItems')
     .then(json => {
       this.setState ({
       card: json.data
      })
    })
  }
  
  render(){
    return (
      <div>
        {this.state.card.map((todo) => (
          <Card employee={todo} />
        ))
        }
      </div>
      	)
      }
	 }

export default ListCard;