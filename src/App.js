import React from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import ListCard from './Container/ListCard';
import Form from './Component/Form';


function App() {
  return (
    <Router>
      <div className="wrapper d-flex">
        <div className="sidebar bg-light">
          <nav className="nav flex-column py-4">
            <a href="" className="nav-link">Dashboard</a>
            <a href="" className="nav-link">Today</a>
            <a href="" className="nav-link">Tomorrow</a>
          </nav>
        </div>
        <div className="page-content py-4">
          <section className="container-fluid">
            <div className="title d-flex align-items-center">
              <h2 className="mr-3">Dashboard</h2>
              <a href="" className="btn btn-outline-primary rounded-pill">Tambah</a>
            </div>
            <div className="mt-4">
              <div className="row">
                <div className="col col-md-6 col-xl-3 mb-4">
                  <Switch>
                   <Route exact path='/'>
                     <ListCard/>
                   </Route>
                   <Route path='/add'>
                     <Form/>
                   </Route>
                  </Switch>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </Router>
  )
}

export default App;
