import React from 'react';
import { Card, Button } from 'react-bootstrap';


const Cards = (props) => {
	return(
	<div className="card rounded-lg shadow">
		<div className="card-header d-flex align-items-center justify-content-between">
      <span>Today, 2 pm</span>
     	<a href="" className="btn btn-outline-primary btn-sm">Edit</a>
    </div>
    <div className="card-body">
      <div> <b>{props.employee.kegiatan}</b></div>
			<div> <font size="4px" color="grey"> {props.employee.tanggal} </font></div>
			<div> <font size="4px" color="grey"> {props.employee.jam} </font></div>
			<div> {props.employee.keterangan}</div>
			<div> <b>{props.employee.status}</b></div>
		</div>
		<div className="btn-group">
					  <button type="button" className="btn btn-success btn-sm">Done</button>
		</div>
	</div>
		)
}
		
export default Cards;