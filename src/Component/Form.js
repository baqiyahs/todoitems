import React from 'react';
import axios from 'axios';

	class Form  extends React.Component {
		state = {
			kegiatan: '',
			jam: '',
			tanggal: '',
			keterangan: '',
			status: '',
		}

		handleChangeKegiatan = event => {
			this.setState({
				kegiatan: event.target.value
			})
		}

		handleChangeJam = event => {
			this.setState({
				jam: event.target.value
			})
		}

		handleChangeTanggal = event => {
			this.setState({
				tanggal: event.target.value
			})
		}

		handleChangeKeterangan = event => {
			this.setState({
				keterangan: event.target.value
			})
		}

		handleChangeStatus = event => {
			this.setState({
				status: event.target.value
			})
		}

		handleSubmit = event => {
	    event.preventDefault();

	    const todo = {
	      keterangan: this.state.keterangan,
	      kegiatan: this.state.kegiatan,
	      jam: this.state.jam,
	      tanggal: this.state.tanggal,
	      status: this.state.status,
	    };

	    axios.post('http://192.168.52.85:5000/api/TodoItems', todo)
	      .then(res => {
	        console.log(res);
	        console.log(res.data);
	      })
	  }

		render(){
			return(
		<div>
			<form className="form" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label for="">Kegiatan</label>
          <input type="text" name="kegiatan" className="form-control" onChange={this.handleChangeKegiatan}/>
        </div>
        <div clasNames="form-group">
          <label for="">Tanggal</label>
          <input type="date" name="tangagl" className="form-control" onChange={this.handleChangeTanggal}/>
        </div>
        <div className="form-group">
          <label for="">Jam</label>
          <input type="time" name="jam" className="form-control" onChange={this.handleChangeJam}/>
        </div>
        <div className="form-group">
          <label for="">Status</label>
          <input type="text" name="status" className="form-control" onChange={this.handleChangeStatus}/>
        </div>
        <div className="form-group">
          <label for="">Keterangan</label>
          <input type="text" name="keterangan" className="form-control" onChange={this.handleChangeKeterangan}/>
        </div>
        <div className="form-group">
          <button type="submit" className="btn btn-primary">Simpan</button>
        </div>
      </form>
     </div>
)
	}
}



export default Form;